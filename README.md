# Naloga

## Structure

- API calls - [services](https://gitlab.com/cencho/metronik-naloga/-/tree/main/src/app/services)
- Models/Schema - [schema](https://gitlab.com/cencho/metronik-naloga/-/tree/main/src/app/schema)
- State control - [shared](https://gitlab.com/cencho/metronik-naloga/-/tree/main/src/app/shared)
- Pages - [pages](https://gitlab.com/cencho/metronik-naloga/-/tree/main/src/app/pages)
- Components - [components](https://gitlab.com/cencho/metronik-naloga/-/tree/main/src/app/components)
- Dummy data - [orders.ts](https://gitlab.com/cencho/metronik-naloga/-/blob/main/src/app/orders.ts)
- Route guards - [app.guards.ts](https://gitlab.com/cencho/metronik-naloga/-/blob/main/src/app/app.guards.ts)
