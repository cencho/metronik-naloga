import { Injectable } from '@angular/core';
import jsonOrders from '../orders';
import { Order } from '../schema/order.schema';
import { v4 as uuidv4 } from 'uuid';
import { Store } from '@ngxs/store';
import { OrdersState } from '../shared/orders.state';
import { AppState } from '../shared/app.state';
import { environment as env } from 'src/environments/environment';

// Service for handling Orders from the OMS
@Injectable()
export class OrdersService {
  constructor(private store: Store) {}

  // Get all orders
  getOrders() {
    // Retrieve all orders from the dummy file.
    const orders: Order[] = jsonOrders;

    return orders;
  }

  // Real api example
  async getOrdersReal() {
    // Retrive all orders from real api.
    const response = await fetch('/api/v2/orders', {
      method: 'GET',
      headers: {
        clientToken: this.store.selectSnapshot(AppState.token) ?? '',
        'Content-Type': 'application/json', // If necessary.
      },
      body: JSON.stringify({
        omsId: env.omsId,
      }),
    });
    if (response.status !== 200) throw new Error(response.statusText);
    const data = await response.json();
    const orders: Order[] = data.result;

    return orders;
  }

  // Function to find order by orderId - this is for the dummy data.
  findOrder(orderId: string) {
    const orders: Order[] = jsonOrders;
    let stateOrders: Order[] = [];

    let order = orders.filter((order) => {
      return order.orderId === orderId ? order : null;
    })[0];

    if (typeof order === 'undefined') {
      this.store.select(OrdersState.orders).subscribe((orders) => {
        stateOrders = orders;
      });

      order = stateOrders.filter((order) => {
        return order.orderId === orderId ? order : null;
      })[0];
    }

    return order;
  }

  // How a real api call for finding an order would look like.
  findOrderReal(orderId: string) {
    let order: Order = new Order();
    fetch('/api/v2/orders/' + orderId, {
      method: 'GET',
      headers: {
        clientToken: this.store.selectSnapshot(AppState.token) ?? '',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        omsId: env.omsId,
      }),
    })
      .then((result) => result.json())
      .then((json) => (order = new Order(json.result)))
      .catch((err) => {
        throw new Error(err);
      });
    if (!order) {
      // Show a 404 page.
    }
    return order;
  }

  // Call backend with the provided new order data. Dummy function for dummy data.
  createOrder(order: Order) {
    // Create a new instance of Order for dummy data purposes.
    const newOrder = new Order(order);
    // Adding the orderId and expected completion time to it.
    const orderId = uuidv4();
    newOrder.orderId = orderId;
    newOrder.expectedCompletionTime = Math.floor(
      Math.random() * (10000 - 1000 + 1) + 1000
    );

    return newOrder;
  }

  // Call API to create new order.
  async createOrderReal(order: Order) {
    const { omsId } = env;
    // Ping the api.
    const ping = await this.ping(omsId);

    // Check if ping succeeded.
    if (!ping) throw new Error('Ping did not succeed.');

    // Call api to create new order.
    const newOrderCall = await fetch('/api/v2/orders', {
      method: 'POST',
      headers: {
        clientToken: this.store.selectSnapshot(AppState.token) ?? '',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(order),
    });
    // Check if status is not 200 in case an error occured.
    if (newOrderCall.status !== 200) throw new Error(newOrderCall.statusText);

    // omsId, orderId, expectedCompletionTime from result
    const newOrder = (await newOrderCall.json()).result;

    // Return above properties.
    return newOrder;
  }

  // Call backend to update an order.
  updateOrder(order: Order) {
    let uOrder: Order = new Order(order);

    // Call the real api to update the order
    // fetch('/api/v2/orders?orderId=' + order.orderId, {
    //   // Depending on the API the orderId can be a query param or body field
    //   method: 'PATCH', // or PUT,
    //   headers: {
    //     clientToken: this.store.selectSnapshot(AppState.token) ?? '',
    //     'Content-Type': 'application/json',
    //   },
    //   body: JSON.stringify(order),
    // })
    //   .then((result) => result.json())
    //   .then((json) => (uOrder = json.result))
    //   .catch((err) => {
    //     throw new Error(err);
    //   });

    return uOrder ?? null;
  }

  // Ping the api
  async ping(omsId: string) {
    const ping = await fetch('/api/v2/ping', {
      method: 'POST', // Or GET?
      headers: {
        clientToken: this.store.selectSnapshot(AppState.token) ?? '',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        omsId,
      }),
    });
    if (ping.status !== 200) return false;

    return true;
  }
}
