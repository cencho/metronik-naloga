import { Injectable } from '@angular/core';
import { environment as env } from 'src/environments/environment';

// Hardcoded user credentials.
const hardCreds = {
  name: 'Test User',
  email: 'test@test.com',
  password: 'password',
  clientToken: '101e2b5a-cfb1-11ec-9d64-0242ac120002',
};

// Auth service for basic authentication
@Injectable()
export class AuthService {
  validate(email: string, password: string) {
    // Validating dummy credentials
    const validEmail = email === hardCreds.email;
    const validPass = hardCreds.password === password;

    if (validEmail && validPass) {
      const { password, clientToken, ...user } = hardCreds;
      return {
        user,
        clientToken,
      };
    }
    return null;
  }

  // Mockup of validation on a real api.
  validateReal(email: string, password: string) {
    let auth: any;
    fetch('/api/v2/auth', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        omsId: env.omsId,
        email,
        password,
      }),
    })
      .then((result) => {
        if (result.status === 401) {
          throw new Error('Invalid credentials.');
        }
        return result.json();
      })
      .then((json) => {
        auth = json.result;
      })
      .catch((err) => {
        throw new Error(err);
      });
    return auth;
  }
}
