import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './app.guards';
import { LoginPage } from './pages/login/login.component';
import { CreateOrdersPage } from './pages/orders/create/create.component';
import { OrdersPage } from './pages/orders/orders.component';
import { UpdateOrderPage } from './pages/orders/update/update.component';
import { ViewOrderPage } from './pages/orders/view/view.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/orders',
  },
  { path: 'login', component: LoginPage },
  { path: 'orders', component: OrdersPage, canActivate: [AuthGuard] },
  {
    path: 'orders/create',
    component: CreateOrdersPage,
    canActivate: [AuthGuard],
  },
  {
    path: 'orders/:orderId',
    component: ViewOrderPage,
    canActivate: [AuthGuard],
  },
  {
    path: 'orders/edit/:orderId',
    component: UpdateOrderPage,
    canActivate: [AuthGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
