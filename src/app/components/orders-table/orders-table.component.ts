import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Order } from 'src/app/schema/order.schema';
import { SetOrders } from 'src/app/shared/orders.actions';
import { OrdersState } from 'src/app/shared/orders.state';

@Component({
  selector: 'app-orders-table',
  templateUrl: './orders-table.component.html',
  styleUrls: ['./orders-table.component.css'],
})
export class OrdersTableComponent implements AfterViewInit {
  orders$: Observable<Order[]>;
  ordersPresent$: Observable<boolean>;

  ordersData: MatTableDataSource<Order> = new MatTableDataSource<Order>();

  columnsToDisplay: string[] = [
    'omsId',
    'orderId',
    'expectedCompletionTime',
    'actions',
  ];

  @ViewChild(MatSort) sort!: MatSort;

  constructor(private store: Store) {
    this.orders$ = this.store.select(OrdersState.orders);
    this.ordersPresent$ = this.store.select(OrdersState.ordersPresent);

    // If no orders in orders state dispatch action to retrieve orders.
    this.ordersPresent$.subscribe((val) => {
      if (!val) {
        this.store.dispatch(new SetOrders());
      }
    });

    // Set orders from state to the material table data source.
    this.orders$.subscribe((orders) => {
      this.ordersData.data = orders;
    });
  }

  ngAfterViewInit() {
    // Add sorting to the table
    this.ordersData.sort = this.sort;
  }
}
