import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './components/navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { OrdersTableComponent } from './components/orders-table/orders-table.component';
import { OrdersPage } from './pages/orders/orders.component';
import { OrdersService } from './services/orders.service';
import { NgxsModule } from '@ngxs/store';
import { AuthService } from './services/auth.service';
import { AppState } from './shared/app.state';
import { AuthGuard } from './app.guards';
import { LoginPage } from './pages/login/login.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatMenuModule } from '@angular/material/menu';
import { CreateOrdersPage } from './pages/orders/create/create.component';
import { MatSelectModule } from '@angular/material/select';
import { MatCardModule } from '@angular/material/card';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { OrdersState } from './shared/orders.state';
import { ViewOrderPage } from './pages/orders/view/view.component';
import { UpdateOrderPage } from './pages/orders/update/update.component';

@NgModule({
  declarations: [
    AppComponent,
    OrdersPage,
    NavigationComponent,
    LoginPage,
    CreateOrdersPage,
    OrdersTableComponent,
    ViewOrderPage,
    UpdateOrderPage,
  ],
  imports: [
    NgxsModule.forRoot([AppState, OrdersState]),
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    FormsModule,
    MatMenuModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatCardModule,
    MatSlideToggleModule,
  ],
  providers: [OrdersService, AuthService, AuthGuard],
  bootstrap: [AppComponent],
})
export class AppModule {}
