import { IOrderDetails, OrderDetails } from './order-details.schema';
import { IProduct, Product } from './product.schema';

export interface IOrder {
  omsId: string;
  products: IProduct[];
  orderDetails?: IOrderDetails;
}

export class Order {
  omsId: string = '';

  orderId: string | null = null;

  expectedCompletionTime: number | null = null;

  products: Product[] = [];

  orderDetails?: OrderDetails | null = null;

  constructor(partial: Partial<Order> = {}) {
    Object.assign(this, partial);
  }
}
