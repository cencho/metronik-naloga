export interface IOrderDetails {
  factoryId: string;
  factoryName?: string;
  factoryAddress?: string;
  factoryCountry: string;
  productLineId: string;
  productCode: string;
  productDescription: string;
  poNumber?: string;
  expectedStartDate?: string;
}

export class OrderDetails {
  factoryId: string = '';

  factoryName?: string;

  factoryAddress?: string;

  factoryCountry: string = '';

  productionLineId: string = '';

  productCode: string = '';

  productDescription: string = '';

  poNumber?: string;

  expectedStartDate?: string;
}
