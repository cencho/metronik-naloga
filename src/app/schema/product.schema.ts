export interface IProduct {
  gtin: string;
  quantity: number;
  serialNumberType: string;
  serialNumbers: string[];
  templateId: string;
}

export class Product {
  gtin: string = '';

  quantity: number = 1;

  serialNumberType: string = '';

  serialNumbers: string[] = [];

  templateId: string = '';
}
