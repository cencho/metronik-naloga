import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Store } from '@ngxs/store';
import { SignIn } from 'src/app/shared/app.actions';

@Component({
  selector: 'page-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginPage implements OnInit {
  // Init new Form Control fields for sign in with hardcoded creds.
  email = new FormControl('test@test.com', [
    Validators.required,
    Validators.email,
  ]);
  password = new FormControl('password', [Validators.required]);

  // Inject the store to dispatch actions and snackbar for user feedback.
  constructor(private store: Store, private snackbar: MatSnackBar) {}

  // Sign in method - check if input is valid and dispatch state action to sign in.
  signIn(e: SubmitEvent) {
    e.preventDefault();

    // Check if errors and inform user.
    if (this.email.invalid || this.password.invalid) {
      return this.snackbar.open('Please enter valid credentials!', 'Close', {
        duration: 3000,
        horizontalPosition: 'right',
      });
    }

    // If there are no errors dispatch the login action on the state.
    return this.store.dispatch(
      new SignIn(this.email.value, this.password.value)
    );
  }

  ngOnInit(): void {}
}
