import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngxs/store';
import { Order } from 'src/app/schema/order.schema';
import { OrdersService } from 'src/app/services/orders.service';
import { UpdateOrder } from 'src/app/shared/orders.actions';
import { environment as env } from 'src/environments/environment';

@Component({
  selector: 'pages-orders-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css'],
})
export class UpdateOrderPage implements OnInit {
  orderId: string = '';
  order: Order;

  orderGroup: FormGroup;
  productGroup: FormGroup;
  orderDetailsGroup: FormGroup;

  countries: any[] = [];

  constructor(
    private fb: FormBuilder,
    private store: Store,
    private ordersService: OrdersService,
    private route: ActivatedRoute
  ) {
    // Get orderId param from URL
    this.route.params.subscribe((params) => {
      this.orderId = params['orderId'];
    });

    // Find order via ordersService with given orderId
    this.order = this.ordersService.findOrder(this.orderId);

    // Form group for grouping and validating input. Default values are taken from order.
    // Products form group.
    this.productGroup = this.fb.group({
      gtin: new FormControl('', [
        Validators.required,
        Validators.minLength(14),
        Validators.maxLength(14),
      ]),
      quantity: new FormControl(1, Validators.required),
      serialNumberType: new FormControl('SELF_MADE', Validators.required),
      serialNumbers: new FormControl('', Validators.required),
    });

    // Order details form group.
    this.orderDetailsGroup = this.fb.group({
      factoryId: new FormControl(this.order.orderDetails?.factoryId),
      factoryName: new FormControl(this.order.orderDetails?.factoryName),
      factoryAddress: new FormControl(this.order.orderDetails?.factoryAddress),
      factoryCountry: new FormControl(this.order.orderDetails?.factoryCountry),
      productionLineId: new FormControl(
        this.order.orderDetails?.productionLineId
      ),
      productCode: new FormControl(this.order.orderDetails?.productCode),
      productDescription: new FormControl(
        this.order.orderDetails?.productDescription
      ),
      poNumber: new FormControl(this.order.orderDetails?.poNumber),
      expectedStartDate: new FormControl(
        this.order.orderDetails?.expectedStartDate
      ),
    });

    // Main form group.
    this.orderGroup = this.fb.group({
      orderId: this.orderId,
      expectedCompletionTime: this.order.expectedCompletionTime,
      omsId: new FormControl(
        { value: env.omsId, disabled: true },
        Validators.required
      ),
      products: this.fb.array(
        this.productsFromOrder(),
        Validators.minLength(1)
      ),
      orderDetails: this.orderDetailsGroup,
    });
  }

  // Form submit function
  updateOrder() {
    // Check if any errors in form based on validation rules.
    if (this.orderGroup.valid) {
      this.serialNumbersToArray();
      // Dispatch action to update order.
      return this.store.dispatch(
        new UpdateOrder(this.orderGroup.getRawValue())
      );
    }
    this.orderGroup.markAsDirty();

    return console.log(this.orderGroup.valid);
  }

  serialNumbersToArray() {
    const products = this.products();
    // Change serial numbers from string to string array.
    for (let group of products.controls) {
      if (group instanceof FormGroup) {
        let serNums = group.controls['serialNumbers'];
        let serNumsArr = serNums.value.split(',');
        serNums.setValue(serNumsArr);
      }
    }
  }

  // Check if orderDetails are empty when submitting.
  orderDetailsEmpty() {
    const filledFields = Object.keys(this.orderDetailsGroup.controls).filter(
      (key) => {
        return this.orderDetailsGroup.controls[key].value !== '';
      }
    );

    return filledFields.length === 0 ?? false;
  }

  // Get all products from order and make form groups of them for use in the form array.
  productsFromOrder() {
    const products = this.order.products.map((product) => {
      // Default values are taken from order.
      return this.fb.group({
        gtin: new FormControl(product.gtin, [
          Validators.required,
          Validators.minLength(14),
          Validators.maxLength(14),
        ]),
        quantity: new FormControl(product.quantity, Validators.required),
        serialNumberType: new FormControl(
          product.serialNumberType,
          Validators.required
        ),
        serialNumbers: new FormControl(
          product.serialNumbers.join(','),
          Validators.required
        ),
      });
    });

    return products;
  }

  // Create new form group in the dynamic form array of 'products'.
  newProduct(): FormGroup {
    return this.fb.group({
      gtin: new FormControl('', [
        Validators.required,
        Validators.minLength(14),
        Validators.maxLength(14),
      ]),
      quantity: new FormControl(1, Validators.required),
      serialNumberType: new FormControl('SELF_MADE', Validators.required),
      serialNumbers: new FormControl('', Validators.required),
    });
  }

  // Retrieve the form arry of form groups 'products' from main 'orderGroup'.
  products() {
    return this.orderGroup.get('products') as FormArray;
  }

  // Retrieve the form group 'orderDetails' from main 'orderGroup'.
  orderDetails() {
    return <FormGroup>this.orderGroup.get('orderDetails');
  }

  // Add product to dynamic form array.
  addProduct() {
    return this.products().push(this.newProduct());
  }

  // Remove product from dynamic form array.
  removeProduct(index: number) {
    return this.products().removeAt(index);
  }

  ngOnInit(): void {
    // Fetch all countries from an open API.
    fetch(
      'https://u50g7n0cbj.execute-api.us-east-1.amazonaws.com/v2/countries',
      {
        method: 'GET',
      }
    )
      .then((result) => result.json())
      .then((countries) => (this.countries = countries.results));
  }
}
