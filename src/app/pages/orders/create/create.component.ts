import { Component, OnInit } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Store } from '@ngxs/store';
import { CreateOrder } from 'src/app/shared/orders.actions';
import { environment as env } from 'src/environments/environment';

@Component({
  selector: 'pages-orders-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
})
export class CreateOrdersPage implements OnInit {
  orderGroup: FormGroup;
  productGroup: FormGroup;
  orderDetailsGroup: FormGroup;

  enableDetails: boolean = false;

  countries: any[] = [];

  constructor(private fb: FormBuilder, private store: Store) {
    // Form group for grouping and validating input.
    // Products form group.
    this.productGroup = this.fb.group({
      gtin: new FormControl('', [
        Validators.required,
        Validators.minLength(14),
        Validators.maxLength(14),
      ]),
      quantity: new FormControl(1, Validators.required),
      serialNumberType: new FormControl('SELF_MADE', Validators.required),
      serialNumbers: new FormControl('', Validators.required),
    });

    // Order details form group.
    this.orderDetailsGroup = this.fb.group({
      factoryId: new FormControl(''),
      factoryName: new FormControl(''),
      factoryAddress: new FormControl(''),
      factoryCountry: new FormControl(''),
      productionLineId: new FormControl(''),
      productCode: new FormControl(''),
      productDescription: new FormControl(''),
      poNumber: new FormControl(''),
      expectedStartDate: new FormControl(''),
    });

    // Main form group.
    this.orderGroup = this.fb.group({
      omsId: new FormControl(
        { value: env.omsId, disabled: true },
        Validators.required
      ),
      products: this.fb.array([this.newProduct()], Validators.minLength(1)),
      orderDetails: this.orderDetailsGroup,
    });
  }

  // Form submit function
  createOrder() {
    // Check if any errors in form based on validation rules.
    if (this.orderGroup.valid) {
      this.serialNumbersToArray();
      // Dispatch action to create new order.
      return this.store.dispatch(
        new CreateOrder(this.orderGroup.getRawValue(), this.orderDetailsEmpty())
      );
    }
    return this.orderGroup.markAsDirty();
  }

  serialNumbersToArray() {
    const products = this.products();
    // Change serial numbers from string to string array.
    for (let group of products.controls) {
      if (group instanceof FormGroup) {
        let serNums = group.controls['serialNumbers'];
        let serNumsArr = serNums.value.split(',');
        serNums.setValue(serNumsArr);
      }
    }
  }

  // Set validators based on whether order details will be included or not.
  setDetailsValidators() {
    const exclude = [
      'factoryName',
      'factoryAddress',
      'poNumber',
      'expectedStartDate',
    ];
    const orderDetails = this.orderDetails();

    if (this.enableDetails) {
      Object.keys(orderDetails.controls).map((key) => {
        if (!exclude.includes(key)) {
          console.log(key);
          orderDetails.controls[key].addValidators(Validators.required);
          orderDetails.controls[key].updateValueAndValidity();
        }
      });
    } else {
      Object.keys(orderDetails.controls).map((key) => {
        if (!exclude.includes(key)) {
          orderDetails.controls[key].removeValidators(Validators.required);
          orderDetails.controls[key].updateValueAndValidity();
        }
      });
    }
  }

  // Check if orderDetails are empty when submitting.
  orderDetailsEmpty() {
    const filledFields = Object.keys(this.orderDetailsGroup.controls).filter(
      (key) => {
        return this.orderDetailsGroup.controls[key].value !== '';
      }
    );

    return filledFields.length === 0 ?? false;
  }

  // Create new form group in the dynamic form array of 'products'.
  newProduct(): FormGroup {
    return this.fb.group({
      gtin: new FormControl('', [
        Validators.required,
        Validators.minLength(14),
        Validators.maxLength(14),
      ]),
      quantity: new FormControl(1, Validators.required),
      serialNumberType: new FormControl('SELF_MADE', Validators.required),
      serialNumbers: new FormControl('', Validators.required),
    });
  }

  // Retrieve the form arry of form groups 'products' from main 'orderGroup'.
  products() {
    return <FormArray>this.orderGroup.get('products');
  }

  // Retrieve the form group 'orderDetails' from main 'orderGroup'.
  orderDetails() {
    return <FormGroup>this.orderGroup.get('orderDetails');
  }

  // Add product to dynamic form array.
  addProduct() {
    return this.products().push(this.newProduct());
  }

  // Remove product from dynamic form array.
  removeProduct(index: number) {
    return this.products().removeAt(index);
  }

  ngOnInit(): void {
    // Fetch all countries from an open API.
    fetch(
      'https://u50g7n0cbj.execute-api.us-east-1.amazonaws.com/v2/countries',
      {
        method: 'GET',
      }
    )
      .then((result) => result.json())
      .then((countries) => (this.countries = countries.results));
  }
}
