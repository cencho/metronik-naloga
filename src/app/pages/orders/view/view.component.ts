import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Order } from 'src/app/schema/order.schema';
import { OrdersService } from 'src/app/services/orders.service';

@Component({
  selector: 'page-orders-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css'],
})
export class ViewOrderPage implements OnInit {
  orderId: string = '';
  order: Order;

  constructor(
    private route: ActivatedRoute,
    private ordersService: OrdersService
  ) {
    // Get orderId param from URL
    this.route.params.subscribe((params) => {
      this.orderId = params['orderId'];
    });

    // Find order via ordersService with given orderId
    this.order = this.ordersService.findOrder(this.orderId);
  }

  ngOnInit(): void {}

  checkIfArray(val: any) {
    return Array.isArray(val);
  }
}
