import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Store } from '@ngxs/store';
import { AppState } from './shared/app.state';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private store: Store, private router: Router) {}
  // Route guard - only authenticated users can visit.
  canActivate() {
    const isAuthenticated = this.store.selectSnapshot(AppState.isAuthenticated);

    if (!isAuthenticated) return this.router.parseUrl('/login');

    return true;
  }
}
