import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Actions, ofActionDispatched, Store } from '@ngxs/store';
import { SetUser, SignOut } from './shared/app.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'metronik-naloga';

  // Inject global store.
  constructor(private store: Store) {}

  ngOnInit(): void {
    // Get auth key from local storage and check if is not null.
    // TODO: Add time limit on stored credentials.
    const auth = localStorage.getItem('auth');
    if (auth !== null) {
      // Dispatch action to set the user in app auth state.
      this.store.dispatch(new SetUser(JSON.parse(auth)));
    }
  }
}
