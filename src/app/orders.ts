/**
 * Dummy data
 */

import { Order } from './schema/order.schema';

export default <Order[]>[
  {
    omsId: '0645848',
    orderId: '5d2f70be-eefb-4128-9e4e-93bbde4f36c0',
    expectedCompletionTime: 3131,
    products: [
      {
        gtin: '35123521351123',
        quantity: 2,
        serialNumberType: 'SELF_MADE',
        serialNumbers: ['3g1313g13e', '324ge3gg23'],
        templateId: '3',
      },
      {
        gtin: '35634563435112',
        quantity: 1,
        serialNumberType: 'SELF_MADE',
        serialNumbers: ['sadgasgddsg'],
        templateId: '3',
      },
    ],
  },
  {
    omsId: '0645848',
    orderId: '356f2935-105b-46ac-b96e-f48d71f38d2a',
    expectedCompletionTime: 1324,
    products: [
      {
        gtin: '35123521351123',
        quantity: 2,
        serialNumberType: 'SELF_MADE',
        serialNumbers: ['3g1313g13e', '324ge3gg23'],
        templateId: '3',
      },
    ],
  },
];
