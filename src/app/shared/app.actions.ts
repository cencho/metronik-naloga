import { AppStateModel } from './app.state';

// Dispatchable SignIn action
export class SignIn {
  static readonly type = '[App] SignIn';
  constructor(public email: string, public password: string) {}
}

// Dispatchable SignOut action
export class SignOut {
  static readonly type = '[App] SignOut';
}

// Dispatchable SetUser action
export class SetUser {
  static readonly type = '[App] SetUser';
  constructor(public userData: AppStateModel['auth']) {}
}
