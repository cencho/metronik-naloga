import { Injectable, NgZone } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { AuthService } from '../services/auth.service';
import { SetUser, SignIn, SignOut } from './app.actions';

// App state interface.
export interface AppStateModel {
  auth: {
    user: {
      name: string;
      email: string;
    } | null;
    clientToken: string | null;
  };
}

// Defaults for the App state.
const defaults: AppStateModel = {
  auth: {
    user: null,
    clientToken: null,
  },
};

// Declare class as a state class.
@State<AppStateModel>({
  name: 'app',
  defaults,
})
@Injectable()
export class AppState {
  constructor(
    private authService: AuthService, // Inject AuthService for user validation.
    private snackbar: MatSnackBar, // Inject Material UI snackbar for user feedback.
    private router: Router, // Inject the router to redirect signed in users.
    private ngZone: NgZone // Inject NgZone for routing purposes.
  ) {}

  // Return the whole auth state.
  @Selector()
  static auth(state: AppStateModel) {
    return state.auth;
  }

  // Return only the user from the state.
  @Selector()
  static user(state: AppStateModel) {
    return state.auth.user;
  }

  // Return only token from the state.
  @Selector()
  static token(state: AppStateModel) {
    return state.auth.clientToken;
  }

  // Check if user is authenticated
  @Selector()
  static isAuthenticated(state: AppStateModel) {
    return !!state.auth.clientToken;
  }

  // Sign in method. Set auth state and save to local storage.
  @Action(SignIn)
  signIn(ctx: StateContext<AppStateModel>, { email, password }: SignIn) {
    const validation = this.authService.validate(email, password);
    if (validation !== null) {
      const authState = {
        user: validation.user,
        clientToken: validation.clientToken,
      };

      // Save user creds to local storage.
      localStorage.setItem('auth', JSON.stringify(authState));
      // Save user creds to state and redirect the user to the orders page.
      return ctx.dispatch(new SetUser(authState));
    }

    // Give user feedback in case of wrong creds.
    return this.snackbar.open('Invalid credentials. Try again.', 'Close', {
      duration: 3000,
    });
  }

  // Sign the user out. Clear state and local storage
  @Action(SignOut)
  signOut(ctx: StateContext<AppStateModel>) {
    localStorage.removeItem('auth');
    ctx.setState({
      auth: {
        user: null,
        clientToken: null,
      },
    });

    // Give user feedback on successfull sign out.
    this.snackbar.open('Signed out successfully!', 'Close', {
      duration: 3000,
    });

    // Redirect user that has logged out to the login page.
    return this.ngZone.run(() => {
      this.router.navigate(['login']);
    });
  }

  // Set the user state and navigate them.
  @Action(SetUser)
  setUser(ctx: StateContext<AppStateModel>, { userData }: SetUser) {
    ctx.setState({
      auth: { ...userData },
    });

    // Give user feedback on succesfull sign in.
    this.snackbar.open('Your are signed in!', 'Close', {
      duration: 3000,
    });

    // Redirect authenticated user to the orders page.
    return this.ngZone.run(() => this.router.navigate(['orders']));
  }
}
