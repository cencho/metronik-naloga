import { Order } from '../schema/order.schema';

// Dispatchable CreateOrder action.
export class CreateOrder {
  static readonly type = '[Orders] Create new order.';
  constructor(public order: Order, public emptyDetails: boolean) {}
}

// Dispatchable SetOrders action.
export class SetOrders {
  static readonly type = '[Orders] Set orders.';
}

// Dispatchable UpdateOrder action.
export class UpdateOrder {
  static readonly type = '[Orders] Update order.';
  constructor(public order: Order) {}
}
