import { Injectable, NgZone } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Action, Selector, State, StateContext } from '@ngxs/store';
import { Order } from '../schema/order.schema';
import { OrdersService } from '../services/orders.service';
import { CreateOrder, SetOrders, UpdateOrder } from './orders.actions';

// Orders state interface
export interface OrdersStateModel {
  orders: Order[];
}

// Default for the orders
const defaults: OrdersStateModel = {
  orders: [],
};

/**
 * Declare class as state class.
 */
@State({
  name: 'orders',
  defaults,
})
@Injectable()
export class OrdersState {
  constructor(
    private ordersService: OrdersService, // Inject OrdersService for order operations.
    private snackbar: MatSnackBar, // Inject Material UI snackbar for user feedback.
    private router: Router, // Inject the router to navigate users after actions.
    private ngZone: NgZone // Inject NgZone for routing purposes.
  ) {}

  // Return orders from the state.
  @Selector()
  static orders(state: OrdersStateModel) {
    return state.orders;
  }

  // Check if orders are empty
  @Selector()
  static ordersPresent(state: OrdersStateModel) {
    return state.orders.length > 0;
  }

  // Set fetched orders in the state.
  @Action(SetOrders)
  setOrders(ctx: StateContext<OrdersStateModel>) {
    const orders: Order[] = this.ordersService.getOrders();
    ctx.setState({
      orders,
    });
  }

  // Create a new order via OrdersService and save it in the state.
  @Action(CreateOrder)
  createOrder(
    ctx: StateContext<OrdersStateModel>,
    { order, emptyDetails }: CreateOrder
  ) {
    // If the order details are empty we delete them as they are optional.
    if (emptyDetails) delete order.orderDetails;

    const newOrder: Order = this.ordersService.createOrder(order);

    if (newOrder) {
      const state = ctx.getState();
      ctx.patchState({
        orders: [...state.orders, newOrder],
      });

      this.snackbar.open('Order created successfully', 'Close', {
        duration: 3000,
      });

      return this.ngZone.run(() => {
        this.router.navigate(['/orders']);
      });
    }
    return this.snackbar.open(
      'An error occured while creating the order.',
      'Close',
      {
        duration: 3000,
      }
    );
  }

  // Update an order.
  @Action(UpdateOrder)
  updateOrder(
    ctx: StateContext<OrdersStateModel>,
    { order: uOrder }: UpdateOrder
  ) {
    const order = this.ordersService.updateOrder(uOrder);

    if (order) {
      const orders: Order[] = ctx.getState().orders;

      const uOrderIndex = orders
        .map((order) => order.orderId)
        .indexOf(uOrder.orderId);

      orders[uOrderIndex] = uOrder;

      ctx.patchState({
        orders,
      });

      this.snackbar.open('Order updated successfully', 'Close', {
        duration: 3000,
      });

      return this.ngZone.run(() => {
        this.router.navigate(['/orders/' + uOrder.orderId]);
      });
    }
    return this.snackbar.open(
      'An error occured while updating the order.',
      'Close',
      {
        duration: 3000,
      }
    );
  }
}
